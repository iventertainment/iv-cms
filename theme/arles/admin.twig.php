<?php

  function page_head( $title ) {
    echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>IV Entertainment</title>
	<link rel="stylesheet" href="images/page/style.css" type="text/css"></head>

<body style="margin-top: 20px;">
<table align="center" width="980" cellspacing="0" cellpadding="0" class="rahmen">
	<tr>
		<td width="191" valign="top" style="background-image:url(images/page/m_bg.png)">
			<img src="images/page/m_header.png" alt="IV Entertainment" border="0">
			<table width="100%" cellpadding="0" cellspacing="0">';
				}

				function page_headpoint( $caption, $link ) {
				echo '<tr><td height="29" class="mhead"><a href="'.$link.'">'.$caption.'</a></td></tr>'."\\n";
				}

				function page_point( $caption, $link ) {
				echo '<tr><td height="17" class="mpoint"><a href="'.$link.'">&raquo; '.$caption.'</a></td></tr>'."\\n";
				}

				function page_content() {
				global $userdata;
				echo '<tr><td><img src="images/page/m_line.png" alt="---" border="0"></td></tr>
			</table><p align="center" style="color:#666666;">'.date( "d.m.Y H:i" ).'</p></td>
		<td style="padding: 15px" valign="top">';
			}

			function page_footer() {
			echo '</td>
	</tr>
</table>
<p align="center">&copy; by IV Entertainment</p>
</body>
</html>';
}

function cbox_auf( $title ) {
echo '<table align="center" width="100%" class="rahmen" cellspacing="0" cellpadding="0">
	<tr>
		<td height="26" style="background-image:url(images/page/c_header.png); padding: 0px 10px 0px 10px"><b>'.$title.'</b></td>
	</tr><tr>
		<td class="cbox">';
			}

			function cbox_zu() {
			echo '</td></tr></table><br>';
}

function msgbox( $msg, $title = "Systemmeldung" ) {
cbox_auf( $title );
echo '<p align="center">'.$msg.'</p>';
cbox_zu();
}

?>