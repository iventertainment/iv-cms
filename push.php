<?php

require 'inc/common.php';
require 'vendor/autoload.php';

use Minishlink\WebPush\WebPush;

$auth = array(
		'VAPID' => array(
				'subject' => $conf->page->url,
				'publicKey' => $conf->system->push_pub_key,
				'privateKey' => $conf->system->push_priv_key,
		),
);

$webPush = new WebPush($auth);


foreach( db()->user_data->get("push_subscription IS NOT NULL") as $user ) {
	$sub = json_decode($user['push_subscription'], true);

	$webPush->sendNotification(
			$sub['endpoint'],
			"Diese Subscription ist aus der datenbank", // optional (defaults null)
			$sub['keys']['p256dh'], // optional (defaults null)
			$sub['keys']['auth'] // optional (defaults null)
	);
}

var_dump($webPush->flush());

