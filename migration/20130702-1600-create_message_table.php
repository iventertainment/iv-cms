<?php

function install() {
	db()->query("
		CREATE TABLE IF NOT EXISTS `user_msg` (
			`id` int(10) unsigned NOT NULL auto_increment,
			`from` int(10) unsigned default NULL,
			`to` int(10) unsigned default NULL,
			`owner` int(10) unsigned NOT NULL,
			`subject` varchar(32) NOT NULL,
			`date` int(11) NOT NULL,
			`content` text NOT NULL,
			`readed` tinyint(1) NOT NULL DEFAULT 0,
			PRIMARY KEY  (`id`),
			KEY `from` (`from`),
			KEY `to` (`to`),
			KEY `owner` (`owner`)
		) ENGINE=InnoDB;" );

	db()->query("ALTER TABLE `user_msg`
		ADD FOREIGN KEY (`from`) REFERENCES `user_data` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
		ADD FOREIGN KEY (`to`) REFERENCES `user_data` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
		ADD FOREIGN KEY (`owner`) REFERENCES `user_data` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;");
}

function remove() {
	db()->query("DROP TABLE `user_msg`;");
}
