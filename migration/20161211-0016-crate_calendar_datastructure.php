<?php

function install() {
	db()->query("CREATE TABLE `calendar_events` (
		  `id` int(10) UNSIGNED NOT NULL,
		  `start` int(10) UNSIGNED NOT NULL,
		  `end` int(10) UNSIGNED NOT NULL,
		  `location` int(10) UNSIGNED NOT NULL,
		  `name` varchar(250) NOT NULL,
		  `description` int(11) NOT NULL,
		  `create_by` int(10) UNSIGNED DEFAULT NULL,
		  `create_date` int(10) UNSIGNED DEFAULT NULL,
		  `update_by` int(10) UNSIGNED DEFAULT NULL,
		  `update_date` int(10) UNSIGNED DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

	db()->query("CREATE TABLE `calendar_locations` (
		  `id` int(10) UNSIGNED NOT NULL,
		  `name` varchar(250) NOT NULL,
		  `active` tinyint(1) DEFAULT 1,
		  `create_by` int(10) UNSIGNED DEFAULT NULL,
		  `create_date` int(10) UNSIGNED DEFAULT NULL,
		  `update_by` int(10) UNSIGNED DEFAULT NULL,
		  `update_date` int(10) UNSIGNED DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

	db()->query("CREATE TABLE `calendar_members` (
		  `id` int(10) UNSIGNED NOT NULL,
		  `event` int(10) UNSIGNED NOT NULL,
		  `comment` text,
		  `create_by` int(10) UNSIGNED NOT NULL,
		  `create_date` int(10) UNSIGNED NOT NULL,
		  `update_by` int(10) UNSIGNED NOT NULL,
		  `update_date` int(11) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
}

function remove() {
	db()->query("DROP TABLE ");
	db()->query("");
	db()->query("");
}
