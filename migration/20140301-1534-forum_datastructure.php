<?php

function install() {
	db()->query("
		CREATE TABLE IF NOT EXISTS `forum_board` (
		  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `name` varchar(100) NOT NULL,
		  `description` varchar(200) DEFAULT NULL,
		  `prio` int(11) NOT NULL DEFAULT 0,
		  `lft` int(10) unsigned DEFAULT NULL,
		  `rgt` int(10) unsigned DEFAULT NULL,
		  `parent` int(10) unsigned DEFAULT NULL,
		  `public_read` tinyint(1) NOT NULL DEFAULT 0,
		  `public_write` tinyint(1) NOT NULL DEFAULT 0,
		  `last_thread` int(10) unsigned DEFAULT NULL,
		  `last_post` int(10) unsigned DEFAULT NULL,
		  `create_date` int(10) unsigned DEFAULT NULL,
		  `create_by` int(10) unsigned DEFAULT NULL,
		  `update_date` int(10) unsigned DEFAULT NULL,
		  `update_by` int(10) unsigned DEFAULT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

	db()->query("
		CREATE TABLE IF NOT EXISTS `forum_thread` (
		  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `board` int(10) unsigned NOT NULL,
		  `title` varchar(200) NOT NULL,
		  `closed` tinyint(1) NOT NULL DEFAULT 0,
		  `pinned` tinyint(1) NOT NULL DEFAULT 0,
		  `last_post` int(10) unsigned DEFAULT NULL,
		  `count_posts` int(10) unsigned DEFAULT NULL,
		  `create_date` int(10) unsigned DEFAULT NULL,
		  `create_by` int(10) unsigned DEFAULT NULL,
		  `update_date` int(10) unsigned DEFAULT NULL,
		  `update_by` int(10) unsigned DEFAULT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

	db()->query("
		CREATE TABLE IF NOT EXISTS `forum_post` (
		  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `thread` int(10) unsigned NOT NULL,
		  `title` varchar(200) NOT NULL,
		  `text_raw` text NOT NULL,
		  `text_cache` text DEFAULT NULL,
		  `create_date` int(10) unsigned DEFAULT NULL,
		  `create_by` int(10) unsigned DEFAULT NULL,
		  `update_date` int(10) unsigned DEFAULT NULL,
		  `update_by` int(10) unsigned DEFAULT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

	db()->query("
		CREATE TABLE IF NOT EXISTS `forum_unread` (
		  `thread` int(10) unsigned NOT NULL,
		  `create_date` int(10) unsigned NOT NULL,
		  `create_by` int(10) unsigned NOT NULL,
		  PRIMARY KEY (`thread`,`create_by`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

	db()->query("ALTER TABLE `forum_board`
		ADD FOREIGN KEY (`parent`) REFERENCES `forum_board`(`id`) ON UPDATE CASCADE ON DELETE CASCADE;");

	db()->query("ALTER TABLE `forum_thread`
		ADD FOREIGN KEY (`board`) REFERENCES `forum_board`(`id`) ON UPDATE CASCADE ON DELETE CASCADE;");

	db()->query("ALTER TABLE `forum_post`
		ADD FOREIGN KEY (`thread`) REFERENCES `forum_thread`(`id`) ON UPDATE CASCADE ON DELETE CASCADE;");

	db()->query("ALTER TABLE `forum_unread`
		ADD FOREIGN KEY (`thread`) REFERENCES `forum_thread`(`id`) ON UPDATE CASCADE ON DELETE CASCADE,
		ADD FOREIGN KEY (`create_by`) REFERENCES `user_data`(`id`) ON UPDATE CASCADE ON DELETE CASCADE;");
}

function remove() {
	db()->query("DROP TABLE `forum_unread`;");
	db()->query("DROP TABLE `forum_post`;");
	db()->query("DROP TABLE `forum_thread`;");
	db()->query("DROP TABLE `forum_board`;");
}
