<?php

function install() {
	db()->query("ALTER TABLE user_data ADD COLUMN push_subscription text DEFAULT NULL");
}

function remove() {
	db()->query("ALTER TABLE user_data DROP COLUMN push_subscription");
}

