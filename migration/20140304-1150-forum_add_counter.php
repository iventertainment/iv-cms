<?php

function install() {
	db()->query("ALTER TABLE `forum_board`
		ADD `count_posts` INT UNSIGNED NOT NULL DEFAULT 0 AFTER `last_post` ,
		ADD `count_threads` INT UNSIGNED NOT NULL DEFAULT 0 AFTER `count_posts`,
		ADD `public_reply` BOOLEAN NOT NULL DEFAULT 0 AFTER `public_write`;");
}

function remove() {
	db()->query("");
}
