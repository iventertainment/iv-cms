<input type="hidden" value="<?= $conf->system->push_pub_key; ?>" id="push-pub-key">
<input type="hidden" value="<?= PAGE_SELF; ?>" id="push-callback-url">
<button id="js-push-btn" disabled>Push notifications are not supportet by your system</button>
<script type="text/javascript" src="/assets/js/push.js"></script>
<?php

if(isset($_POST['subscription']))
	db()->user_data->updateRow(array('push_subscription' => $_POST['subscription']), $user->id);

