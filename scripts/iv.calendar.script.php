<?php

$status = array();
$status[-1] = $status['abgelehnt'] = "#880000";
$status[0]  = $status['unknown']   = "#111111";
$status[1]  = $status['joined']    = "#555500";
$status[2]  = $status['ersatz']    = "#333388";
$status[3]  = $status['approved']  = "#338833";

$rols = array ( "meele", "ranged", "tanks", "healer" );
$classcolors = array( '#AA000', '#009900', '#6666FF', '#AAAA00' );


if( empty( $_GET['event'] )) {
	$planer = new data_calendar();
	$calendar = $planer->data();
	$calendar['self'] = PAGE_SELF;

	$panel['name'] = $panel['name'] .' - '. date( "F Y", $calendar['monatsanfang'] );
	echo template('eso.calendar')->render($calendar);
} elseif( $event = $db->id_get( "eso_events", $_GET['event'] )) {

	$event['start_zeit'] = date( "l - d.m.Y - H:i", $event['start'] + $event['invite'] * 60 );
	$event['end_zeit'] = date( "l - d.m.Y - H:i", $event['start'] + $event['invite'] * 60  + $event['dauer'] * 60  );

	$limit = 2;

	if( $userdata && ( $event['start'] - $limit * 60 > time() )) {
		$anmeldung = $db->fetch_query( "SELECT m.* FROM eso_eventmembers m JOIN eso_chars c ON c.id = m.`char`
                                    WHERE owner = '$userdata[id]' AND event = '$event[id]'" );
		$anmeldung = $anmeldung[0];

		$query = array( "event" => $event['id'], "char" => (int) $_POST['char'], "status" => (int)($_POST['status']!=0), "comment" => $_POST['comment'] );
		if( !empty( $_POST['char'] ))
			if( $anmeldung ) $db->update( "eso_eventmembers", $query, "`event` = '$event[id]' AND `char` = '$anmeldung[char]'" );
			else $db->insert( "eso_eventmembers", $query );

		$form = new form( PAGE_SELF."&event=".$event['id'] );
		$form->add( "char", "Charakter", empty( $anmeldung['char'] ) ? $userdata['wow_char'] : $anmeldung['char'], "select", $db->get_assoc( "eso_chars", "owner = '$userdata[id]'", "name" ) );
		$form->add( "status", "Status", 1, "select", array( "Abmelden", "Anmelden" ));
		$form->add( "comment", "Kommentar", $anmeldung['comment'] );

	}

	$comments = $db->fetch_query( "SELECT * FROM eso_eventmembers m JOIN eso_chars c ON c.id = m.`char` WHERE event = '$event[id]' AND m.comment is not null" );
	$membercount = $db->fetch_query( "SELECT count(*) c FROM eso_eventmembers WHERE event = '$event[id]' AND status != 0" );
	$target = $db->id_get( "eso_inzen", $event['target'] );
	$leader = $db->id_get( "user_data", $event['leader'] );

	$members = $db->fetch_query( "SELECT *
        FROM eso_eventmembers m
        JOIN eso_chars c ON c.id = m.`char`
        WHERE event = '$event[id]' AND m.status != 0
        ORDER BY m.status DESC, m.create_date" );

	foreach( $members as $m ) {
		$m['classcolor'] = $classcolors[$m['classid']];
		$m['statuscolor'] = $status[$m['status']];
		$event[$rols[$m['rolle']]][] = $m;
	}

	$panel['name'] = $target['name'];

	template( 'eso/event' )->display( array(
		"self" => PAGE_SELF.'&event='.$event['id'],
		"event" => $event,
		"status" => $status,
		"target" => $target,
		"leader" => $leader,
		"comments" => $comments,
		"membercount" => $membercount[0]['c'],
		"form" => $form
	));
} else {
	echo 'event nicht gefunden!';
}