<?php

$migrations = new update_migration();

if( !empty( $_POST['create'])) {
	$migrations->create( $_POST['create'] );
	throw new redirect(MODUL_SELF);
}

if( !empty( $_GET['apply'])) {
	db()->update_migration->insert(array( 'id' => $_GET['apply'] ));
	throw new redirect(MODUL_SELF);
}

if( !empty( $_GET['reset'])) {
	db()->update_migration->delRow($_GET['reset']);
	throw new redirect(MODUL_SELF);
}

if( !empty( $_GET['install'] )) {
	$view->format = 'plain';
	$migrations->install( $_GET['install'] );
} else {
	$all = globFiles('migration/*');
	$applied = db()->select('update_migration')->assocs('id');
	$result = $pending =array();

	foreach( $all as $id ) {
		$result[] = array(
			'id' => $id,
			'status' => isset($applied[$id]) ? 'applied' : 'pending',
			'date' => $applied['create_date']
		);

		if( empty( $applied[$id] ))
			$pending[] = $id;
	}

	$grid = $view->grid();
	$view->js('assets/js/migration.js');

	$form = new form_renderer(MODUL_SELF, 'Erstellen');
	$form->text('create', 'Migrationsname');
	$grid[0]->box($form, 'Migration erstellen');

	$grid[0]->box(template('iv.migrations.install')->render(array(
			'pending' => $pending,
	)), 'Migrationen anwenden');

	$list = new list_array( MODUL_SELF );
	$list->text('Datei', 'id');
	$list->text('Status', 'status');
	$list->date('Datum', 'date');
	$list->add($actions = new list_column_actions('Aktionen'));

	$actions->add(MODUL_SELF, 'apply', 'Mark Applied', 'assets/small/check.png');
	$actions->add(MODUL_SELF, 'reset', 'Reset', 'assets/small/undo.png');

	$grid[1]->box($list->get($result), 'Migrationen' );
}
