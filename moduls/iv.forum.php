<?php

function recalcTree( $element ) {
	static $counter;
	if( is_array( $element->children ))
		foreach( $element->children as $child ) {
			$child->left = ++$counter;
			recalcTree( $child );
			$child->right = ++$counter;
		}
}

$boards = db()->select('forum_board')->relate();
$boards[0] = 'System Root';

if( !empty( $_REQUEST['parent'] )) {
	// do not allow cyclic references
	$board = $_GET['update'] ?: $_GET['move'];
	if( db()->query("SELECT 1 FROM forum_board node JOIN forum_board sub
			ON node.lft < sub.lft AND node.rgt > sub.rgt
			WHERE node.id = %d AND sub.id = %d", $board, $_REQUEST['parent'] )->value())
		throw new Exception('You can not do that!');
}

$rc = new data_controller('forum_board', MODUL_SELF);
$rc->add('name', 'Name', 1, 1, 1, 1);
$rc->add('description', 'Beschreibung', 1, 1, 1, 0);
$rc->add('parent', 'Parent', 0, 1, 1, 0, 'select', $boards );

$rc->add('public_read', 'Lesen (Jeder)', 0, 1, 1, 0, 'checkbox');
$rc->add('public_write', 'Schreiben (Jeder)', 0, 1, 1, 0, 'checkbox');
$rc->add('public_reply', 'Antworten (Jeder)', 0, 1, 1, 0, 'checkbox');

$changes = $rc->run();

if( !empty( $_GET['move'] )) {
	db()->id_update('forum_board', array( 'parent' => $_GET['parent'] ), $_GET['move'] );
	$changes = db()->affected_rows;
}

$list = db()->select( 'forum_board' )->objects( 'id' );
$root = new stdClass();

foreach( $list as $board )
	if( $board->parent ) $list[$board->parent]->children[] = $board;
	else $root->children[] = $board;

if( $changes ) {
	// cool datastructure stuff: http://mikehillyer.com/articles/managing-hierarchical-data-in-mysql/
	recalcTree( $root );
	$query = array();

	foreach( $list as $board )
		$query[] = db()->format("(%d, %d, %d)", $board->id, $board->left, $board->right );

	db()->query("INSERT INTO forum_board (id, lft, rgt ) VALUES ".implode(',', $query ).
			" ON DUPLICATE KEY UPDATE lft=VALUES(lft), rgt=VALUES(rgt);");
	throw new redirect(MODUL_SELF);
}

if( !empty( $_GET['editform'] )) {
	$view->content( $rc->get_edit($_GET['editform']));
	$view->format = 'plain';
} else {
	$view->box( template('iv.forum.boardtree')->render(array(
			'tree' => $root,
			'self' => MODUL_SELF,
			'create' => $rc->get_create(),
			'current' => intval( $_GET['edit'] ),
	)), 'Foren');
}
