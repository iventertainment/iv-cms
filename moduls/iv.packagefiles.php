<?php

$packages = update_package::liste();

if(isset($_GET['changes'])) {
	$files = db()->select('update_file')->assocs();

	foreach( $files as $file )
		if($packages[$file['package']]['version'] < $file['version'])
			$packages[$file['package']]['new'][] = $file;
		elseif(!is_file($file['path']))
			$packages[$file['package']]['deleted'][] = $file;
		elseif(update_package::hash( $file['path'] ) != $file['hash'])
			$packages[$file['package']]['changed'][] = $file;

	foreach($packages as $id => $p)
		$packages[$id]['display'] =
				(empty($p['source']) || !empty($p['forward'])) &&
				($p['new'] || $p['changed']);

	$view->content(template('iv.packages.changes')->render(array(
		'packages' => array_values($packages)
	)));
} elseif(!empty($_GET['file'])) {
	$view->format = 'plain';
	$ext = strrchr($_GET['file'], '.');

	if($ext == '.png' || $ext == '.gif' || $ext == '.jpeg' || $ext == '.jpg' )
		echo '<img src="'.$_GET['file'].'">';
	else
		highlight_file($_GET['file']);
} elseif( !empty($_POST['files']) && !empty($_POST['package'])) {
	$pkg = new update_package( $packages[$_POST['package']] );

	foreach( $_POST['files'] as $file )
		if( file_exists( $file ))
			$pkg->addFile( $file );

	if(!empty($_POST['comment']))
		$pkg->changeLog($_POST['comment']);

	throw new redirect( MODUL_SELF.'&added='.count($_POST['files']));
} else {
	if(!empty($_GET['added']))
		$view->success(intval($_GET['added'])." Dateien hinzugefügt.");

	$files = update_manager::get_files();
	$auto = update_manager::get_assignable();
	$suggestions = array();

	foreach( $files as $f )
		if(isset($auto[$f]) && empty($suggestions[$f] ))
			$suggestions[$f] = $auto[$f];

	if(isset($_GET['apply']))  {
		$pkgs = array();

		foreach($packages as $id => $p)
			$pkgs[$id] = new update_package( $p );

		foreach($suggestions as $f => $id)
			$pkgs[$id]->addFile($f);

		throw new redirect( MODUL_SELF.'&added='.count($suggestions));
	} else {
		$view->content(template('iv.packages.files')->render(array(
			'files' => $files,
			'suggestions' => $suggestions,
			'packages' => array_values($packages)
		)));
	}
}