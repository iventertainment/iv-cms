<?php

if( !empty( $_POST )) {
	// Name Validation
	if( empty( $_POST['subject'] ))
		throw new exception_user( 'Bitte Betreff angeben' );
	elseif( empty( $_POST['content'] ))
		throw new exception_user( 'Bitte Nachricht angeben' );
	elseif( strlen( $_POST['content'] ) > 10000 && strlen( $_POST['subject'] ) > 32 )
		throw new exception_user( 'Betreff oder Nachricht zu lang' );
	else {
		$db->query( "INSERT INTO user_msg ( `date`, `owner`, `to`, `from`, `subject`, `content` )
			SELECT UNIX_TIMESTAMP(), id, id, %d, '%s', '%s' FROM user_data",
			$user->id, $_POST['subject'],  $_POST['content'] );
		$db->query("UPDATE user_data SET pns = pns + 1");

		throw new redirect(MODUL_SELF.'&success');
	}
}

if( isset( $_GET['success']))
	$view->box( '<p alifn="center">Rundschreiben erfolgreich versendet!</p>', 'Nachricht erfolgreich' );

$form = new form( MODUL_SELF, 'Senden' );
$form->text( 'subject', 'Betreff' )
	->input( 'class', 'input-xxlarge' )
	->required();
$form->textarea( 'content', 'Nachricht' )
	->input( 'class', 'input-xxlarge' )
	->input( 'rows', 10 )
	->required();

$view->box( $form, 'Rundschreiben verfassen' );
