<?php

if( empty( $_GET['day'] )) {
	$calendar = new data_calendar();
	$data = $calendar->data();

	$data['self'] = MODUL_SELF;
	$data['daylink'] = true;

	$view->box( template('iv.calendar.overview')->render($data), date( 'F Y', $data['monatsanfang'] ));
} elseif( !empty( $_GET['event'] ) && $event = $db->id_get('calendar_events', $_GET['event'] )) {
	if( is_array( $_POST['char'] ) && !empty( $_POST['status'] ))
		foreach( $_POST['char'] as $c )
			$db->update( 'calandar_event_members', array( 'status' => (int) $_POST['status'] ), "`event` = '$event[id]' and `char` = '".intval($c)."'" );

	$chars = $db->fetch_query( "SELECT * FROM eso_chars c 
			JOIN calandar_event_members m ON c.id = m.`char` 
			WHERE event = '$event[id]' AND m.status != 0 
			ORDER BY rolle DESC, m.status DESC, itemlevel DESC" );

	$view->box( template('eso.eventmember')->render(array(
		'chars' => $chars,
		'rollen' => $rollen,
		'status' => $status,
		'event' => $event,
		'day' => intval( $_GET['day'] )
	)), 'Anmeldungen');
} else {
	$locations = $db->get_assoc( 'calendar_locations', 'active = 1', 'name' );

	if( isset( $_POST['start'] )) {
		$start = explode(':', $_POST['start']);
		$_POST['start'] = $_GET['day'] + $start[0] * 3600 + $start[1] * 60;
	}

	$rc =  new data_controller( 'calendar_events', MODUL_SELF.'&day='.$_GET['day'] );
	$rc->add( 'name', 'Titel', 1, 1, 1, 1);
	$rc->add( 'start', 'Startzeitpunkt', 1, 1, 1, 1, 'datetime' );
	$rc->add( 'end', 'Endzeitpunkt', 1, 1, 1, 1, 'datetime' );
	$rc->add( 'location', 'Ort', 1, 1, 1, 1, 'select', $locations );
	$rc->add( 'description', 'Beschreibung', 0, 1, 1, 0, 'textarea' );
	$rc->condition = 'start between '.intval( $_GET['day'] ).' and '.( $_GET['day'] + 3600 * 24 );
	$rc->option('assets/small/users.png', 'event', 'Anmeldungen');

	if( $rc->run()) throw new redirect( MODUL_SELF.'&day='.$_GET['day'] );

	$grid = new widget_grid(array(5, 7));
	$grid[1]->box( $rc->get_list(), date( 'l - d.m.Y', $_GET['day'] ).' - events');
	$grid[0]->box( $rc->get_form(), date( 'l - d.m.Y', $_GET['day'] ).' - event anlegen & bearbeiten');
	$view->content( $grid.'<br><p align="center"><a class="btn" href="'.MODUL_SELF.'">Zurück</a></p>' );

}