self.addEventListener('push', function(event) {
	var fallback = 'Nothing to see here.';
	var title = 'IV CMS Forum Notification';

	var payload = event.data ? event.data.text() : fallback;

	var options = {
		body: payload,
		// icon: 'img/icon.png',
		// badge: 'img/badge.png'
	};

	event.waitUntil(self.registration.showNotification(title, options));
});
