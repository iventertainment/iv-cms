const applicationServerPublicKey = document.querySelector('#push-pub-key').value;
const applicationServerUrl = document.querySelector('#push-callback-url').value;
const pushButton = document.querySelector('#js-push-btn');

var isSubscribed = false;
var swRegistration = null;

function urlB64ToUint8Array(base64String) {
	const padding = '='.repeat((4 - base64String.length % 4) % 4);
	const base64 = (base64String + padding).replace(/\-/g, '+').replace(/_/g, '/');

	const rawData = window.atob(base64);
	const outputArray = new Uint8Array(rawData.length);

	for (var i = 0; i < rawData.length; ++i) {
		outputArray[i] = rawData.charCodeAt(i);
	}

	return outputArray;
}

function updateBtn() {
	if (Notification.permission === 'denied') {
		pushButton.textContent = 'Push Messaging Blocked.';
		pushButton.disabled = true;
		updateSubscriptionOnServer(null);
		return;
	}

	pushButton.textContent = isSubscribed ? 'Disable Push Messaging' : 'Enable Push Messaging';
	pushButton.disabled = false;
}

function updateSubscriptionOnServer(subscription) {
	var subJson = JSON.stringify(subscription);
	$.post(applicationServerUrl, {subscription: subJson});
}

function subscribeUser() {
	const applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey);
	swRegistration.pushManager.subscribe({
		userVisibleOnly: true,
		applicationServerKey: applicationServerKey
	}).then(function(subscription) {
		updateSubscriptionOnServer(subscription);
		isSubscribed = true;
		updateBtn();
	}).catch(function(err) {
		console.log(err);
		updateBtn();
	});
}

function unsubscribeUser() {
	swRegistration.pushManager.getSubscription().then(function(subscription) {
		if (subscription)
			return subscription.unsubscribe();
	}).then(function() {
		updateSubscriptionOnServer(null);
		isSubscribed = false;
		updateBtn();
	});
}

function initialiseUI() {
	pushButton.addEventListener('click', function() {
		pushButton.disabled = true;

		if (isSubscribed) {
			unsubscribeUser();
		} else {
			subscribeUser();
		}
	});

	// Set the initial subscription value
	swRegistration.pushManager.getSubscription().then(function(subscription) {
		isSubscribed = !(subscription === null);
		updateBtn();
	});
}

if ('serviceWorker' in navigator && 'PushManager' in window) {
	navigator.serviceWorker.register('/assets/js/service-worker.js').then(function(swReg) {
		swRegistration = swReg;
		initialiseUI();
	}).catch(function(error) {
		console.log(error);
	});
} else {
	pushButton.textContent = 'Push Not Supported';
}
