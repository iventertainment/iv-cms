<?php

	function db() { return $GLOBALS['db']; };

	if( file_exists( 'inc/ftp.config.php' )) {
		require_once 'inc/ftp.config.php';
		$writer = new writer_ftp( $ftp_host, $ftp_user, $ftp_pass, $ftp_dir );
	} else {
		$writer = new writer_fs();
	}

	if( isset( $_GET['storefile'] )) {
		$pkg = new update_package(array( 'id' => $_POST['pkg'] ));
		$pkg->updateFile( $_POST['path'], $_POST['content'] );
		die('ok');
	} elseif( isset( $_GET['addpackage'] )) {
		$pkg = new update_package( $_POST );
		$pkg->install( $writer );
		die('ok');
	} elseif( isset( $_GET['getmigrations'] )) {
		die(json_encode( update_migration::listPending()));
	} elseif( !empty( $_GET['install'] )) {
		$migrations = new update_migration();
		$migrations->install( $_GET['install'] );
		die();
	}
