<?php

$template = file_get_contents( 'install/template.php' );

if( isset( $argv[1] ))
	$template = str_replace('mysql', 'sqlite', $template );

// Replace includes by file content
while( preg_match_all("/(require|include)\s*ROOT\.'([^']+)'\s*;/is", $template, $matches, PREG_SET_ORDER ))
	foreach( $matches as $m ) {
		$replace = trim( substr( file_get_contents ( $m[2]), 5 ))."\n";
		$template = str_replace ( $m[0], $replace, $template);
	}

// Remove doc comments
$template = preg_replace('|\s/\*.*?\*/|is', ' ', $template);
// Remove commented lines
$template = preg_replace('|^\s*//.*$|m', '', $template);
// Remove spaces
$template =  preg_replace( '/\s{2,}/is', ' ', $template );

echo $template;
