<?php

$server = array(
	'name' => 'Official Update Server',
	'url' => 'http://iv-cms.de/'
);

/*
$server = array(
	'name' => 'Localhost',
	'url' => 'http://localhost/iv-cms/'
);
*/

$packages = array(
		'iv.user',
		'iv.core',
		'iv.cms',
		'iv.twig',
		'iv.theme.bootstrap',
		'iv.style.default',
		'iv.update',
		'iv.codemirror'
);

// Error Reporting Konfigurieren
define( 'ERROR_LEVEL', E_ALL ^ E_NOTICE);
define( 'ROOT', dirname( __FILE__ ).'/../' );
error_reporting(ERROR_LEVEL);

@ini_set( 'display_errors', 1 );
@ini_set( 'register_globals', 'off' );

function errorExceptionHandler($errno, $errstr, $errfile, $errline) {
	if( $errno & ERROR_LEVEL )
		throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
}

set_error_handler('errorExceptionHandler');
header( 'Content-Type: text/html; charset=UTF-8' );

require ROOT.'inc/functions.php';
require ROOT.'lib/writer/fs.php';
require ROOT.'lib/writer/ftp.php';
require ROOT.'lib/update/package.php';
require ROOT.'lib/update/migration.php';

require ROOT.'lib/mysql/result.php';
require ROOT.'lib/mysql/exception.php';
require ROOT.'lib/mysql/table.php';
require ROOT.'lib/mysql/connection.php';

define( 'IV_SELF', 'install.php?' );

if( file_exists( 'inc/database.config.php' )) {
	require_once 'inc/database.config.php';

	if( isset( $_GET['complete'] )) {
		include ROOT.'install/complete.php';
		include ROOT.'install/step3.tpl.php';
	} else {
		include ROOT.'install/packages.php';
		include ROOT.'install/step2.tpl.php';
	}
} else {
	include ROOT.'install/init.mysql.php';
	include ROOT.'install/writeconfig.mysql.php';
	include ROOT.'install/step1.tpl.php';
}
