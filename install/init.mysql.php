<?php

function initDb( $db ) {
	// Install basic database
	$db->query("CREATE TABLE IF NOT EXISTS `update_server` (
				`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`name` varchar(100) NOT NULL,
				`url` text NOT NULL,
				`update_date` int(10) unsigned DEFAULT NULL,
				`update_by` int(10) unsigned DEFAULT NULL,
				`create_date` int(10) unsigned DEFAULT NULL,
				`create_by` int(10) unsigned DEFAULT NULL,
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8;");

	$db->query("CREATE TABLE IF NOT EXISTS `update_package` (
				`id` varchar(200) NOT NULL,
				`source` int(10) unsigned DEFAULT NULL,
				`version` int(10) unsigned NOT NULL,
				`update_date` int(10) unsigned DEFAULT NULL,
				`update_by` int(10) unsigned DEFAULT NULL,
				`create_date` int(10) unsigned DEFAULT NULL,
				`create_by` int(10) unsigned DEFAULT NULL,
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

	$db->query("CREATE TABLE IF NOT EXISTS `update_file` (
				`path` varchar(255) NOT NULL,
				`package` varchar(200) NOT NULL,
				`version` int(10) unsigned DEFAULT NULL,
				`hash` varchar(32) NULL,
				`content` LONGBLOB NULL,
				PRIMARY KEY (`path`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

	$db->query("CREATE TABLE IF NOT EXISTS `update_migration` (
				`id` varchar(250) NOT NULL,
				`create_date` int(10) unsigned DEFAULT NULL,
				`create_by` int(10) unsigned DEFAULT NULL,
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

	$db->query("CREATE TABLE IF NOT EXISTS `update_dependency` (
				`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`package` varchar(200) NOT NULL,
				`required` varchar(200) NOT NULL,
				`version` int(10) unsigned NOT NULL,
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

	$db->query("CREATE TABLE IF NOT EXISTS `update_share` (
				`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`package` varchar(200) NOT NULL,
				`comment` varchar(200) DEFAULT NULL,
				`pattern` varchar(200) NOT NULL,
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8;");
}
